<?php
session_start();
require_once 'include/db.php';
require_once 'classes/user.php';    // Will make sure the user gets logged in
if ($user->isLoggedIn()) // i.e. the user has just logged in, transfer the user to the main page
    header ("Location: index.php");
?>
<!doctype html>
<html lang="">
  <head>
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Velkommen til undervisningsvideor på nett</title>

    <link rel="apple-touch-icon" href="images/favicon.png">
    <link rel="icon" type="image/png" href="images/favicon.png">
    <!-- Place favicon.ico in the root directory -->

    <!-- build:css styles/vendor.css -->
    <!-- bower:css -->
    <link rel="stylesheet" href="../bower_components/bootstrap/dist/css/bootstrap.css" />
    <!-- endbower -->
    <!-- endbuild -->

    <!-- build:css styles/main.css -->
    <link rel="stylesheet" href="styles/main.css">
    <!-- endbuild -->
    <script>
        var menuItemSelected = "login";
    </script>
    
  </head>
  <body>
    <!--[if lt IE 10]>
      <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
    
    <?php 
        require_once 'include/topMenu.php'; 
        $user->getLoginForm();  // Show the login form
        require_once 'include/bottomScriptIncludes.html';
    ?>
  </body>
</html>