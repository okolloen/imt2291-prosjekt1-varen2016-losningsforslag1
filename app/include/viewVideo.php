<!-- Included in index.php to show a video if one is selected -->
<?php
// The video class is used to insert the actuall video tag with source and tracks
require_once 'classes/video.php';
?>
<div class="container-fluid">
	<div class="row" style="margin-right: 5px; margin-left: 5px">
		<div class="col-xs-8" id="videoDisplay" style="border: 1px solid lightgrey">
			<?php $video->createVideoTag($_GET['video']); ?>
		</div>
		<div class="col-xs-4" style="box-shadow: 3px 3px 13px 0px rgba(0,0,0,0.5);overflow-y: auto; border: 1px solid lightgrey; margin-bottom: 2px;" id="annotations">
		</div>
	</div>
</div>
<script>
$(function() {	// Forcing the height of the annotations div to be correct
	$('#annotations').height($('#videoDisplay').height());	// Initial height
	$(window).resize(function() {	// Window is resized
		$('#annotations').height($('#videoDisplay').height());
	});
	$('#videoDisplay video').on('resize', function() {	// The video itself is resized
		$('#annotations').height($('#videoDisplay').height());
	}); 
});
</script>