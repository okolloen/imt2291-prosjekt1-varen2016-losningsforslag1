<!-- 
Included in classes/video.php to show a form that allows the user 
to upload annotations/text tracks -->
<style type="text/css">
	.fileUpload {
	    position: relative;
	    overflow: hidden;
	    margin: 10px;
	}
	.fileUpload input.upload {
	    position: absolute;
	    top: 0;
	    right: 0;
	    margin: 0;
	    padding: 0;
	    font-size: 20px;
	    cursor: pointer;
	    opacity: 0;
	    filter: alpha(opacity=0);
	}
</style>
<div class="panel panel-default">
    <div class="panel-heading"><h3 class="panel-title">Last opp en fil med annoteringer/teksting</h3></div>
    <div class="panel-body" style="margin-top: 10px;">
		<form method="post" action="editVideo.php?video=<?php echo $_GET['video']; ?>" enctype="multipart/form-data">
			<div class="form-group">
			  	<label for="kind">Type</label>
			  	<select name="kind" class="form-control" placeholder="Hvilken type annotering/teksting laster du opp" value="subtitles">
			  		<option value="subtitles">Inneholder undertekster (standard)</option>
			  		<option value="caption">Inneholder all dialog og lydeffekter (passer for hørselshemmede</option>
			  		<option value="chapters">Inneholder kapittelnavn (passer for å kunne navigere i filmen)</option>
			  		<option value="descriptions">Inneholder en tekstlig beskrivelse av innholdet (passer for synshemmede)</option>
			  		<option value="metadata">Inneholder data, ment for datamaskinen</option>
			  	</select>
			</div>
			<div class="form-group">
			    <label for="label">Navn</label>
			    <input required type="text" id="label" name="label" class="form-control" placeholder="Navnet/tittelen på sporet">
			</div>
			<div class="form-group">
			    <label for="srclang">Språk</label>
			    <input required type="text" id="srclang" name="srclang" class="form-control" placeholder="no - for norsk, en - for engelsk">
			</div>
			<div class="checkbox">
			    <label>
			    	<input type="checkbox" name="default" value="true"> Default (brukes som standard)
			    </label>
			</div>
			<div class="form-group">
				<input id="uploadFile" class="form-control" style="width: 80%; float: left;" placeholder="Velg fil" disabled="disabled" />
				<div class="fileUpload btn btn-primary" style="margin-top:0px">
					<span>Velg fil</span>
					<input required name="file" type="file" id="file" class="upload" />
				</div>
			</div>
			<div class="form-group">
				<input type="submit" name="addAnnotation" class="btn btn-primary" value="Last opp fil"/>
			</div>
		</form>
	</div>
</div>
<script>
$(function () {
	// When the actuall file element changes then update the one visible to the user
	$('#file').on('change', function(event) {
		$('#uploadFile').val($('#file').val());
	});
});
</script>