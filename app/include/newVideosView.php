<!-- Included in index.php if no user is logged in, shows new videos/playlists -->
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12 col-lg-6">
		    <div class="panel panel-default">
		        <div class="panel-heading">
		        	<h3 class="panel-title">Nye videoer</h3>
		        </div>
	            <div class="panel-body">
	            	<?php
	            		// The video class is used to generate a list of new videos
	            		require_once 'classes/video.php';
	            		$video->createVideoListTable("SELECT id, title, description, duration FROM video ORDER BY tstamp DESC LIMIT 100", array ());
	            	?>
	           	</div>
		    </div>
		</div>
		<div class="col-xs-12 col-lg-6">
		    <div class="panel panel-default">
		        <div class="panel-heading">
		        	<h3 class="panel-title">Nye spillelister</h3>
		        </div>
	            <div class="panel-body">
		            <?php
			            // The playlist class is used to generate a list of new playlists
		                require_once ('classes/playlist.php');
		                $playlist->generatePlaylist ('SELECT id, title, description FROM playlist ORDER BY tstamp DESC LIMIT 50', array ()); 
	                ?> 
	           	</div>
		    </div>
		</div>
	</div>
</div>