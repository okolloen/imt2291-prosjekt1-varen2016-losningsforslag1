<?php
// Included everywhere that a database connection is needed (all php pages)
$dbname = 'imt2291_prosj1';	// Name of the database
$dbuser = 'imt2291_prosj1';	// Username for access to the database
$dbpwd  = 'imt2291_prosj1'; // Password for access to the database

try {
	$db = new PDO('mysql:host=127.0.0.1;dbname='.$dbname, $dbname, $dbpwd);
} catch (PDOException $e) {
	// If install is set we want to continue running code
	if (!isset($install)&&isset($debug))
	    die ('Kunne ikke koble til serveren : ' . $e->getMessage());
	else if (!isset($install))
		die ('Kunne ikke koble til databasen, prøv igjen senere.');
}