<?php
session_start();
require_once 'include/db.php';
require_once 'classes/user.php';
?>
<!doctype html>
<html lang="">
  <head>
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Velkommen til undervisningsvideor på nett</title>

    <link rel="apple-touch-icon" href="images/favicon.png">
    <link rel="icon" type="image/png" href="images/favicon.png">
    <!-- Place favicon.ico in the root directory -->

    <!-- build:css styles/vendor.css -->
    <!-- bower:css -->
    <link rel="stylesheet" href="../bower_components/bootstrap/dist/css/bootstrap.css" />
    <!-- endbower -->
    <!-- endbuild -->

    <!-- build:css styles/main.css -->
    <link rel="stylesheet" href="styles/main.css">
    <!-- endbuild -->

    <script src="../bower_components/jquery/dist/jquery.js"></script>
    
  </head>
  <body>
    <!--[if lt IE 10]>
      <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
    
    <?php 
        require_once 'include/topMenu.php'; 
        require_once 'classes/video.php';

        // Only allow editing of users own videos
        if (!$video->mine||!isset($_GET['video'])) { ?>
            <div class="alert alert-danger" role="alert">
                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                <span class="sr-only">Ingen tillatelse:</span>
                Du har ikke rettigheter til å redigere denne videoen?
            </div> <?php
        } else {
            $editing = true;
            // Creates the video tag with source and track info
            // The add to playlist option will also be created here
            include_once ('include/viewVideo.php');

            // Show a form below the video to enable uploading of text tracks
            echo '<div class="container-fluid" style="margin-top: 10px"><div class="row">';
            echo '<div class="col-xs-12 col-lg-6">';
            $video->createUploadAnnotationsForm();
            echo '</div>';
            echo '<div class="col-xs-12 col-lg-6">';
            echo '</div>';
            echo '</div></div>';
            
        } 

        require_once 'include/bottomScriptIncludes.html';
    ?>

  </body>
</html>
