<?php
session_start();
require_once 'include/db.php';
require_once 'classes/user.php';
?>
<!doctype html>
<html lang="">
  <head>
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Velkommen til undervisningsvideor på nett</title>

    <link rel="apple-touch-icon" href="images/favicon.png">
    <link rel="icon" type="image/png" href="images/favicon.png">
    <!-- Place favicon.ico in the root directory -->

    <!-- build:css styles/vendor.css -->
    <!-- bower:css -->
    <link rel="stylesheet" href="../bower_components/bootstrap/dist/css/bootstrap.css" />
    <!-- endbower -->
    <!-- endbuild -->

    <!-- build:css styles/main.css -->
    <link rel="stylesheet" href="styles/main.css">
    <!-- endbuild -->

    <script src="../bower_components/jquery/dist/jquery.js"></script>
    
  </head>
  <body>
    <!--[if lt IE 10]>
      <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
    
    <?php 
        require_once 'include/topMenu.php'; 

        // Check to see if a playlist id is given
        if (isset($_GET['id'])) { ?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Videoer i spillelisten : <?php
                        $owner = -1;
                        // Get the title and owner of the playlist
                        $sql = "SELECT title, owner FROM playlist WHERE id=?";
                        $sth = $db->prepare ($sql);
                        $sth->execute (array ($_GET['id']));
                        if ($row = $sth->fetch()) {
                            echo $row['title'];
                            $owner = $row['owner'];
                        }
                    ?></h3> <?php
                        // If currently logged in user is owner
                        // Show link to edit the playlist
                        if ($owner===$user->getUID()) {
                            echo "<a href='editPlaylist.php?id={$_GET['id']}' title='Rediger spilleliste'><span style='float: right; margin-top: -15px' class='glyphicon glyphicon-pencil' aria-hidden='true'></span></a>";
                        }
                    ?>
                </div>
                <div class="panel-body"> 
                <?php
                    // Use functionality in the Video class to 
                    // generate the list of videos in the playlist
                    require_once 'classes/video.php';
                    // Generic video list generating method
                    // Takes the SQL and param array as parameters
                    $video->createVideoListTable("SELECT id, title, description, duration FROM video, playlistItem WHERE pid=? and vid=id ORDER BY sortorder", array ($_GET['id']));
                ?> 
                </div>
            </div><?php
        }

        require_once 'include/bottomScriptIncludes.html';
        if ($user->isLoggedIn()) { ?>
            <script src="../bower_components/dynatable/jquery.dynatable.js"></script>
            <script>
                // When page is loaded, transform the table with videos
                // into a sortable, searchable, paged table
                $(function () {
                    $('#videos').dynatable();
                });
            </script> <?php
        }
    ?>

  </body>
</html>
