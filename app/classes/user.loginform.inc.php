<!-- 
Code for signin/signup form was copied from http://bootsnipp.com/snippets/featured/login-amp-signup-forms-in-panel 
Adapted for this project by Øivind Kolloen, January 2014

This is just the login form
-->

<div class="container">    
    <div id="loginbox" style="margin-top:50px;" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">                    
        <div class="panel panel-info" >
            <div class="panel-heading">
                <div class="panel-title">Logg inn</div>
                <div style="float:right; font-size: 80%; position: relative; top:-10px"><a href="#">Glemt passordet?</a></div>
            </div>     

            <div style="padding-top:30px" class="panel-body" >

				<?php echo $this->alert; ?>
                    
                <form id="loginform" class="form-horizontal" role="form" method="post" action="<?php echo $_SERVER["REQUEST_URI"]; ?>">
                            
                    <div style="margin-bottom: 25px" class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
                        <input id="login-username" type="email" class="form-control" name="email" <?php echo $email; ?> placeholder="E-post adressen din">                                        
                    </div>
                        
                    <div style="margin-bottom: 25px" class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                        <input id="login-password" type="password" class="form-control" name="password" placeholder="Passord">
                    </div>
                        
                    <div class="input-group">
                        <div class="checkbox">
                            <label>
                                <input id="login-remember" type="checkbox" name="remember" value="1"> Husk meg
                            </label>
                        </div>
                    </div>

                    <div style="margin-top:10px" class="form-group">
                        <!-- Button -->

                        <div class="col-sm-12 controls">
                            <input type="submit" id="btn-login" class="btn btn-success" value="Login"/>

                        </div>
                    </div>
                </form>     
            </div>                     
        </div>  
    </div>
</div>