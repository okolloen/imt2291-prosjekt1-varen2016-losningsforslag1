<?php 
/**
 * This class is used for assorted manipulations of playlists.
 *
 * @author Øivind Kolloen
 *
 */
class Playlist {
	var $db;

	/**
	 * Takes a reference to the database as a parameter.
	 * If $_POST['newPlaylistName'] exists a new playlist will be created.
	 * If $_POST['addToPlaylist'] exists the a video will be added to selected playlist.
	 *
	 * @param db a reference to the database object
	 */
	function Playlist ($db) {
		$this->db = $db;
		if (isset ($_POST['newPlaylistName'])) {
			// Creates a new playlist
			$this->createPlaylist($_POST['newPlaylistName']);
		} else if (isset ($_POST['addToPlaylist'])) {
			// Add video to playlist
			$this->addToPlaylist($_GET['video'], $_POST['addToPlaylist'], isset($_POST['isThumbnail']));
		}
	}

	/**
	 * This method is used to generate a playlist table at the location
	 * where this method is called. The HTML code is inserted directly into
	 * the output stream.
	 *
	 * @param sql the sql expression to execute
	 * @param param the parameters to send to the database with the sql
	 * to get the desired result
	 */
	function generatePlaylist ($sql, $param) {
		global $user; ?>
    	<table id="playlists" class="table table-striped table-hover">
            <thead>
                <th>Tittel</th><th style="width:75%">Beskrivelse</th>
            </thead>
            <tbody> <?php
            	$sth = $this->db->prepare ($sql);
            	$sth->execute ($param);
            	while ($row = $sth->fetch(PDO::FETCH_ASSOC)) {
            		echo "<tr><td><b><a href='playlist.php?id={$row['id']}'>{$row['title']}<b><br/><img src='api/playlistThumbnail.php?id={$row['id']}' class='img-responsive img-rounded'/></a></td><td>";
            		if (isset($row['owner'])) {
            			echo "<a href='editPlaylist.php?id={$row['id']}' title='Rediger spilleliste'><span style='float: right;' class='glyphicon glyphicon-pencil' aria-hidden='true'></span></a>";
            		}
            		echo "{$row['description']}</td></tr>";
            	} ?>
            </tbody>
        </table> <?php
	}

	/**
	 * This method is used to create a new blank playlist.
	 * No description or thumbnail image for the playlist is created.
	 * Takes the name of the playlist to be created as a parameter, 
	 * gets the user object from the global namespace.
	 *
	 * @param name the name of the playlist to create.
	 */
	function createPlayList ($name) {
		global $user;
		if ($user->isLoggedIn()) {
			$sql = "INSERT INTO playlist (owner, title, description) VALUES (?, ?, ?)";
			$sth = $this->db->prepare ($sql);
			$sth->execute (array ($user->getUID(), $name, ""));
		}
	}

	/**
	 * This method is used to create a "add to playlist option"
	 * The user will  be presented with a drop down box to select the 
	 * desired playlist.
	 * The actuall adding to the playlist is done in the constructor
	 * of this class.
	 * 
	 */ 
	function createAddToPlaylist () {
		global $user;
        $sql = 'SELECT id, title FROM playlist WHERE owner=?';
        $sth = $this->db->prepare ($sql);
        $sth->execute (array ($user->getUID()));
        if ($row = $sth->fetch()) {
            echo '<form class="form-inline" method="post" action="" style="float: right"><div class="form-group"><label for="addToPlaylist">Velg spilleliste &nbsp;</label><select style="width:200px;" id="addToPlaylist" class="form-control" name="addToPlaylist">';
            echo "<option value='{$row['id']}'>{$row['title']}</option>";
            while ($row = $sth->fetch()) {
                echo "<option value='{$row['id']}'>{$row['title']}</option>";
            }
            echo '</select></div> &nbsp; <div class="checkbox"><label><input type="checkbox" name="isThumbnail" value="true"> Bruk som thumbnail</label></div>';
            echo ' &nbsp; <input type="submit" value="Legg til i spilleliste" class="btn btn-primary"></form>';
        }
	}

	/**
	 * This method is used to add a video to a playlist and 
	 * optionally use its thumbnail as a thumbnail for the playlist.
	 *
	 * @param video the id of the video to add
	 * @param playlist the id of the playlist to add this video to
	 * @param isThumbnail use the thumbnail for this video as the
	 * thumbnail for this playlist.
	 */
	function addToPlaylist($video, $playlist, $isThumbnail) {
		global $user;
		if ($isThumbnail) {	// If this video should be thumbnail
			// Figure out the url for the api/thumbnail script
			$url = $_SERVER['HTTP_REFERER'];
			$url = substr ($url, 0, strrpos($url, "/"));
			$full = file_get_contents("$url/api/thumbnail.php?id=$video");
			$img = imagecreatefromstring($full);	// Read the video thumbnail

			$width = imagesx($img);			// Find the width
			$height = imagesy($img);		// Fint the height
			$scaleX = $width/200;
			$scaleY = $height/200;
			
			$scale = ($scaleX>$scaleY)?$scaleX:$scaleY;	// What axis needs the most scaling
			$scale = ($scale>1)?$scale:1;				// Do not scale UP
			
			$scaled = imagecreatetruecolor($width/$scale, $height/$scale);

			// Scale the image
			imagecopyresampled($scaled, $img, 0, 0, 0, 0, $width/$scale, $height/$scale, $width, $height);
			ob_start();							// Start and clean the buffer
			imagejpeg ($scaled, null, 100);		// Send the image to the browser
			$thumb = ob_get_contents();
			ob_end_clean();						// Get contents of buffer

			// Update playlist thumbnail image
			$sql = "UPDATE playlist SET thumbnail=? WHERE id=? AND owner=?";
			$sth = $this->db->prepare ($sql);
			$sth->execute (array ($thumb, $playlist, $user->getUID()));
		}
		// Insert video into playlist and give it sortorder 1 (one) higher than the highest value for sortorder for that playlist (0 if first item in playlist)
		$sql = "INSERT INTO playlistItem (vid, pid, sortorder) select ?, ?, MAX(sortorder)+1 FROM playlistItem WHERE pid=?";
		$sth = $this->db->prepare ($sql);
		$sth->execute (array ($video, $playlist, $playlist))
		// Give the user feedback that the video was added to the playlist
		?>
		<div class="alert alert-success" role="alert">
            <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
            <span class="sr-only">Suksess:</span>
            Videoen er lagt til i spillelisten
        </div>
        <script>
        	$(function() {
        		// Fade the feedback out and then remove it
        		$('div[role="alert"]').fadeOut(5000);
        	});
        </script> <?php
	}
}

$playlist = new Playlist($db);