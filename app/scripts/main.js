// Run this when the page has completed loading
$(function() {	
	// Make the correct menu element the selected one
	if (typeof menuItemSelected !== 'undefined') {
		// Set the menu element with this data-info as the active one
		$('[data-info="'+menuItemSelected+'"]').addClass('active');
	}
});
