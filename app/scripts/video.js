// This file is included by the video player and contains functionality
// for displaying and updating the text cue information.
// NOTE: this is included AFTER the video tag is inserted into the page.

// Get the text tracks from the video element
var htmlTracks = document.querySelectorAll("track");

// Converts nl character into br tag (to show linebreaks in the text tracks)
function nl2br(str) {
  //  discuss at: http://phpjs.org/functions/nl2br/
  // original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  // improved by: Philip Peterson
  // improved by: Onno Marsman
  // improved by: Atli Þór
  // improved by: Brett Zamir (http://brett-zamir.me)
  // improved by: Maximusya
  // bugfixed by: Onno Marsman
  // bugfixed by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  // Simplified by: Øivind Kolloen
  //    input by: Brett Zamir (http://brett-zamir.me)
  //   example 1: nl2br('Kevin\nvan\nZonneveld');
  //   returns 1: 'Kevin<br />\nvan<br />\nZonneveld'
  //   example 2: nl2br("\nOne\nTwo\n\nThree\n", false);
  //   returns 2: '<br>\nOne<br>\nTwo<br>\n<br>\nThree<br>\n'
  //   example 3: nl2br("\nOne\nTwo\n\nThree\n", true);
  //   returns 3: '<br />\nOne<br />\nTwo<br />\n<br />\nThree<br />\n'

  return (str + '')
    .replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + '<br/>' + '$2');
}

// Loop trough all text tracks (current version of the code only handles
// the default track of kind==subtitles )
for (var i=0; i<htmlTracks.length; i++) {
	var currentHtmlTrack = htmlTracks[i];
  var currentTextTrack = currentHtmlTrack.track;
  // We want the default subtitle track
  if (currentTextTrack.kind === 'subtitles' && currentHtmlTrack.default) {
  	getTrack (currentHtmlTrack); // Loads this track
  }
}

// This function loads the track and then calls a function to get
// information from the track
function getTrack(htmlTrack) {
  var textTrack = htmlTrack.track;
  
  if(htmlTrack.readyState === 2) {  // Track already loaded
     textTrack.mode = "hidden";     // Do not overlay the cues on the video
     readContent(textTrack);        // Handle information in the track
  } else {
     textTrack.mode = "hidden";     // Force loading of the track and do not overlay the cues on the video
     htmlTrack.addEventListener('load', function(e) { // Set on load handler
         readContent(textTrack);    // Handle information in the track
    });
  }
}

// Gets all cues from the track, display them beside the video and sets up
// listeners for the cues
function readContent (track) {
  // Add unnumbered list to the annotations div
	$('#annotations').html('<ul style="padding-top:5px;padding-bottom:5px;"></ul>');
	var annotations = $('#annotations ul');
	var cues = track.cues;
  
	// iterate on the cue list
 	for(var i=0; i < cues.length; i++) {
    	// current cue
   		var cue = cues[i];
   		addCueListeners(cue);  // Add listeners for entering/leaving this cue
    
    	var text = cue.text;
    
      // Add a li for this cue, add the start and end time as data- attributes
    	annotations.append ('<li data-starttime="'+cue.startTime+'" data-endtime="'+cue.endTime+'">' + nl2br(text)+'</li>');
  	}

    // Call this function when the users clicks on a cue
  	$('#annotations ul li').on ('click', function() {
      // Set the video to start playing from the start time of this cue
  		$('video')[0].currentTime = this.dataset['starttime'];
  	});
}

// Function to add listeners to a cue. Add onEnter and onExit listeners
function addCueListeners (cue) {
	cue.onenter = function(){  // Called when the video reaches the time where this cue starts
  		$('li[data-starttime="'+this.startTime+'"]').addClass ('activeCue');  // Mark the text of this cue as active (highlight it)

      // Calculate the scrollTop of the surrounding box for this cue to be centered
      var center = $('li[data-starttime="'+this.startTime+'"]').parent().parent()[0].offsetHeight/2;
      var elTop = $('li[data-starttime="'+this.startTime+'"]')[0].offsetTop;
      if (elTop > center) { // We should scroll
        $('li[data-starttime="'+this.startTime+'"]').parent().parent()[0].scrollTop = elTop-center;
      } else {              // Keep scrollTop at 0
        $('li[data-starttime="'+this.startTime+'"]').parent().parent()[0].scrollTop = 0;
      }
  	};
  	
  	cue.onexit = function(){ // Called when the video reaches the endTime for this cue
  		$('li[data-starttime="'+this.startTime+'"]').removeClass('activeCue');  // Remove hightlighting of this cue
  	};
}