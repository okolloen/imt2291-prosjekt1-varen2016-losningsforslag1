<?php
$install = true;
require_once '../include/db.php';
if (isset($db))
	die ("Prosjektet er allerede installert");
if (isset($_POST["db_username"])) {
	try {
		$db = new PDO('mysql:host=127.0.0.1;', $_POST['db_username'], $_POST['db_pwd']);
	} catch (PDOException $e) {
		die ("Kunne ikke koble til databaseserveren : " . $e->getMessage());
	}
	$sql = "CREATE USER '$dbuser'@'localhost' IDENTIFIED BY '$dbpwd';GRANT USAGE ON *.* TO '$dbuser'@'localhost' REQUIRE NONE WITH MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0;CREATE DATABASE IF NOT EXISTS `$dbname`;GRANT ALL PRIVILEGES ON `$dbname`.* TO '$dbuser'@'localhost';";
	$db->exec($sql);
	$db->exec('FLUSH PRIVILEGES');

	// Add the user table
	$sql = "CREATE TABLE `imt2291_prosj1`.`user` ( `id` BIGINT NOT NULL AUTO_INCREMENT , `uname` VARCHAR(128) NOT NULL , `pwd` VARCHAR(255) NOT NULL , `fullName` VARCHAR(128) NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB CHARACTER SET utf8;";
	$db->exec($sql);
	// Add column for admin status
	$sql = "ALTER TABLE `imt2291_prosj1`.`user` ADD `admin` ENUM('y','n') NOT NULL DEFAULT 'n' AFTER `pwd`;";
	$db->exec($sql);

	// Insert admin user
	$sql = 'INSERT INTO `imt2291_prosj1`.user (uname, pwd, admin, fullName) VALUES ("imt2291@ntnu.no", ?, "y", "Administrator")';
	$sth = $db->prepare ($sql);
	$sth->execute(array(password_hash('imt2291', PASSWORD_DEFAULT)));

	// Add persistant login table
	$sql = 'CREATE TABLE `imt2291_prosj1`.`persistantLogin` ( `uid` BIGINT NOT NULL , `identifier` CHAR(32) NOT NULL , `token` CHAR(32) NOT NULL , `tstamp` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP)';
	$db->exec($sql);
	$sql = 'ALTER TABLE `imt2291_prosj1`.`persistantLogin` ADD UNIQUE `uid_identifier` (`uid`, `identifier`);';
	$db->exec($sql);

	// Add video table
	$sql = 'CREATE TABLE `imt2291_prosj1`.`video` ( `id` BIGINT NOT NULL AUTO_INCREMENT , `owner` BIGINT NOT NULL , `title` VARCHAR(128) NOT NULL , `description` TEXT NOT NULL , `filename` VARCHAR(255) NOT NULL , `filepath` VARCHAR(255) NOT NULL , `mimetype` VARCHAR(128) NOT NULL , PRIMARY KEY (`id`))';
	$db->exec($sql);
	// Add a timestamp to the video table, needed to be able to show latest uploaded videos
	$sql = 'ALTER TABLE `imt2291_prosj1`.`video` ADD `tstamp` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `mimetype`;';
	$db->exec($sql);
	$sql = 'ALTER TABLE `imt2291_prosj1`.`video` ADD `duration` FLOAT NULL AFTER `tstamp`;';
	$db->exec($sql);

	// Add playlist table
	$sql = 'CREATE TABLE `imt2291_prosj1`.`playlist` ( `id` BIGINT NOT NULL AUTO_INCREMENT , `owner` BIGINT NOT NULL , `title` VARCHAR(128) NOT NULL , `description` TEXT NOT NULL , PRIMARY KEY (`id`))';
	$db->exec($sql);
	$sql = 'ALTER TABLE `imt2291_prosj1`.`playlist` ADD `tstamp` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `description`, ADD `thumbnail` BLOB NULL AFTER `tstamp`;';
	$db->exec($sql);

	// Add table for storing support material for video (text tracks, thumbnails etc)
	$sql = 'CREATE TABLE `imt2291_prosj1`.`videoAddons` ( `vid` BIGINT NOT NULL COMMENT "Video id" , `mime` VARCHAR(255) NOT NULL , `content` MEDIUMBLOB NOT NULL COMMENT "text track/thumbnail")';
	$db->exec($sql);	
	$sql = 'ALTER TABLE `imt2291_prosj1`.`videoaddons` ADD `extras` TEXT NOT NULL COMMENT "Extra information, json encoded" AFTER `mime`;';
	$db->exec($sql);
	$sql = 'ALTER TABLE `imt2291_prosj1`.`videoaddons` ADD UNIQUE `vidExtras` (`vid`, `extras`(255))';
	$db->exec($sql);
	$sql = 'ALTER TABLE `imt2291_prosj1`.`videoaddons` ADD `id` BIGINT NOT NULL AUTO_INCREMENT FIRST, ADD PRIMARY KEY (`id`)';
	$db->exec($sql);

	// Add table for storing items in playlists
	$sql = 'CREATE TABLE `imt2291_prosj1`.`playlistItem` ( `vid` BIGINT NOT NULL COMMENT "Video id" , `pid` BIGINT NOT NULL COMMENT "Playlist id" )';
	$db->exec ($sql);
	$sql = 'ALTER TABLE `imt2291_prosj1`.`playlistItem` ADD UNIQUE `vidPid` (`vid`, `pid`);';
	$db->exec ($sql);
	$sql = 'ALTER TABLE `imt2291_prosj1`.`playlistItem` ADD `sortorder` INT NOT NULL DEFAULT `1` AFTER `pid`;';
	$db->exec ($sql);

	// Add table for logging of video viewing
	$sql = 'CREATE TABLE `imt2291_prosj1`.`log` ( `vid` BIGINT NOT NULL , `uid` BIGINT NOT NULL , `tstamp` TIMESTAMP NOT NULL , `length` FLOAT NOT NULL COMMENT "For future use, tracking of how much of a video a user has seen" )';
	$db->exec ($sql);

	echo 'Databasen er installert, gå til <a href="../">hovedsiden</a> og logg inn med "imt2291@ntnu.no" som brukernavn og "imt2291" som passord for å få administratortilgang';
	die();
}
?>
<!DOCTYPE html>
<html>
<head>
<title>Installere prosjektet</title>
</head>
<body>
<h2>Installere prosjektet</h2>
<p>Opprett katalogen uploads under app katalogen, gi alle brukere skriverettighter til denne katalogen. Opprett så katalogen temp under uploads og gjør det samme med denne.</p>
<p>Installer ffmpeg/ffprobe i katalogen ffmpeg ved siden av app katalogen. Eventuelt oppdater plasseringen av ffmpeg/ffprobe i api/receiveFile.php</p>
<p>Oppgi brukernavn og passord for root for å opprette databasen og brukeren for systemet.</p>
<form method="post" action="index.php">
<label for="db_username">Root brukernavn</label><input type="text" name="db_username" id="db_username" value="root"/><br/>
<label for="db_pwd">Root passord</label><input type="password" name="db_pwd" id="db_pwd"/><br/>
<input type="submit" value="Opprett database og bruker"/>
</form>
</body>
</html>