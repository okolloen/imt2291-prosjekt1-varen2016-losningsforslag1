<?php
/**
 * Skriptet kalles fra upload.php og tar i mot en fil fra en bruker. Filen legges 
 * midlertidig i katalogen uploads/temp.
 * Når brukeren legger inn tittel og beskrivelse for filen vil den flyttes over i 
 * katalogen uploads og kobles mot en post i databasen.
 *
 * Katalogen uploads/temp bør tømmes for gamle filer av og til (dersom en bruker 
 * ikke legger inn tittel/beskrivelse så vil filene bli liggende igjen i 
 * uploads/temp katalogen.)
 */
$_apache = apache_request_headers();	// Ekstra informasjon ligger i disse, må hentes spesielt
$fn = (isset($_apache['X_FILENAME']) ? $_apache['X_FILENAME'] : false);
$mime = (isset($_apache['X_MIMETYPE']) ? $_apache['X_MIMETYPE'] : false);
$size = (isset($_apache['X_FILESIZE']) ? $_apache['X_FILESIZE'] : false);

$fn = tempnam(getcwd() . "/../uploads/temp", "VID_");
$name = substr($fn, strrpos ($fn, DIRECTORY_SEPARATOR)+1);

header ("Content-type: application/json");	// Vi sender svaret som json data
if ($fn) {									// Dersom en fil er mottatt

	// AJAX call
	file_put_contents(						// Ta i mot filen og lagre den i uploads katalogen
		$fn,					// Her må en ta høyde for dupliserte filnavn
		file_get_contents('php://input')
	);

	// Use ffprobe to get the duration of the video (in seconds.thousands)
	$cmd = "../../ffmpeg/ffprobe -v error -show_entries format=duration -of default=noprint_wrappers=1:nokey=1 ../uploads/temp/$name 2>&1";
	$cmd = join (DIRECTORY_SEPARATOR, explode ('/',$cmd));	//	Use "\" on windows
	if (DIRECTORY_SEPARATOR==='/') {						// prepend this on linux/mac
		$cmd = "DYLD_LIBRARY_PATH='' && ".$cmd;
	}
	exec ($cmd, $res, $code);


	$duration = $res[0];
	$thirds = $duration/3;	// We want to generate three thumbnails
	$frameRate = 1/$thirds;
	$offset = $thirds/2;	// generate the thumbnails half way into each third

	// Use ffmpeg to generate the thumbnails. Same location/name as the video 
	// but add 001.jpg/002.jpg/003.jpg to the name
	$cmd = "../../ffmpeg/ffmpeg -i ../uploads/temp/$name -ss $offset -vf fps=$frameRate -r $frameRate ../uploads/temp/$name"."_%03d.jpg 2>&1";
	$cmd = join (DIRECTORY_SEPARATOR, explode ('/',$cmd)); 	// User "\" on windows
	if (DIRECTORY_SEPARATOR==='/') {						// prepend this on linux/mac
		$cmd = "DYLD_LIBRARY_PATH='' && ".$cmd;
	}
	exec ($cmd, $res, $code);	// We execute the OS independent command
	$thumbs= array ($name.'_001.jpg', $name.'_002.jpg', $name.'_003.jpg');
	if (file_exists ("../uploads/temp/$name"."_004.jpg")) {		// Sometimes three images get generated, mostly four
		array_push ($thumbs, $name.'_004.jpg');
	}

	// json encode and send back an array with information
	echo json_encode(array('ok'=>'OK', 'filename'=>$fn, 'name'=>$name, 'duration'=>$duration, 'thumbs'=>$thumbs));	// Send svar til klienten
}