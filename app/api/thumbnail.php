<?php
// Script used to return a thumbnail (screenshot) for a video
// This is used when displaying a list of videos

require_once '../include/db.php';

// Anyone can get the thumbnail for any video
$sql = "SELECT mime, content FROM videoaddons WHERE vid=?";
$sth = $db->prepare ($sql);
$sth->execute (array($_GET['id']));
if ($row = $sth->fetch()) {		// This should always be true, no fallback
	header ('Content-type: '.$row['mime']);		// Just in case we have a non jpeg thumbnail
	echo $row['content'];		// Send the content of the thumbnail back
} 