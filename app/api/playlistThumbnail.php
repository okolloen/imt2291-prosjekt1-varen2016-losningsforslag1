<?php
// This script is used to get thumbnails for/from the playlist

require_once '../include/db.php';

// Anyone can get any thumbnail
$sql = "SELECT thumbnail FROM playlist WHERE id=?";
$sth = $db->prepare ($sql);
$sth->execute (array($_GET['id']));
if ($row = $sth->fetch()) {		// If thumbnail found, 
	if ($row['thumbnail']!=null) {				// There is a thumbnail
		header ('Content-type: image/jpeg');	// Will always be jpeg format
		echo $row['thumbnail'];					// Send the thumbnail
	} else {									// No thumbnail for this list
		header ('Content-type: image/png');		// Send a transparent png image
		readfile ('../images/blank.png');		// The image to send
	}
} 