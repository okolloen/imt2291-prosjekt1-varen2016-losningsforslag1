<?php
// This script is used to get vtt text tracks from the database

require_once '../include/db.php';

header ('Content-type: text/vtt');	// Only used for vtt text tracks

// Anyone can get any text track
$sql = 'SELECT content FROM videoaddons WHERE id=?';
$sth = $db->prepare ($sql);
$sth->execute (array($_GET['id']));
if ($row = $sth->fetch())
	echo $row['content'];	// Send the track back to the client